/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

//Changement d'idée : on fait appel a requestKeepAlive dans goto comme ça c'est plus "léger et dynamique"
// ons.ready(function () {
//   document.addEventListener('deviceready', function () {
//     setInterval(requestKeepAlive, globalTenMinuts);
//   });
// });

function requestKeepAlive() {
  // myDebug("requestKeepAlive :" + globalNetworkResponse);
  //Si jamais une requête est ok dans le code ailleurs
  if ((Date.now() - globalNetworkResponse) > (globalTenMinuts + 1000)) {
    // myDebug("requestKeepAlive : ca fait trop longtemps ... on fait un ping");
    // myDebug("ping host :" + localGetData("api_server"));
    api_token = localGetData("api_token");
    //au cas ou
    if (ajaxKeepAlive)
      ajaxKeepAlive.abort();

    var ajaxKeepAlive = $.ajax({
      url: localGetData("api_server") + "/api/ping",
      timeout: 5000,
      type: "POST",
      dataType: "json",
      data: {
        email: localGetData("email"),
      },
      headers: {
        Authorization: 'Bearer ' + api_token
      },
      //ok
      success: function (result, textStatus, request) {
        hideWait();
        myDebug("requestKeepAlive : ping ok (" + textStatus + "/" + request.status + ")");
        globalNetworkResponse = Date.now();
      },
      //logout
      error: function (result) {
        hideWait();
        myDebug("requestKeepAlive : ping err -> end session");
        localStoreData("api_token", null);
        gotoPage('login.html');
      }
    });
  }
}
