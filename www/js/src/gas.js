/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

function rmGas(storageKey, id) {
    let gasList = JSON.parse(localGetData(storageKey));
    gasList.splice(id, 1);
    if (gasList.length > 0)
        localStoreData(storageKey, JSON.stringify(gasList));
    else
        localStoreData(storageKey, null);
}

function addGas(storageKey, oxy, he, ppo2, uuid = null) {
    if (uuid === null || uuid == '' || uuid == undefined) {
        let label = oxy + ';' + he + ';' + ppo2;
        uuid = Math.abs(label.hashCode());
    }
    myDebug("Add gas for uuid=" + uuid);

    return new Promise((resolve, reject) => {
        let gasNew = { "uuid": uuid, "oxy": oxy, "he": he, "ppo2": ppo2 };
        let v = localGetData(storageKey);
        let gasList = null;
        let duplicate = false;
        if (null !== v && v != "") {
            gasList = JSON.parse(v);
        }
        if (gasList === null) {
            gasList = [];
        } else {
            gasList.forEach(function (item) {
                if (item.oxy == oxy && item.he == he && item.ppo2 == ppo2) {
                    duplicate = true;
                    myDebug("Duplicate for " + item.oxy);
                }
            });
        }
        myDebug("duplicate gas " + duplicate);
        if (duplicate === false) {
            gasList.push(gasNew);
            myDebug("Add gas " + oxy);
            resolve(localStoreData(storageKey, JSON.stringify(gasList)));
        }
    });
}

function findGas(storageKey, uuid) {
    let gasList = JSON.parse(localGetData(storageKey));
    if (gasList === null) {
        gasList = [];
    } else {
        gasList.forEach(function (item) {
            if (item.uuid == uuid) {
                let gas = { "uuid": uuid, "oxy": item.oxy, "he": item.he, "ppo2": item.ppo2 };
                return gas;
            }
        });
    }
}

/**
 * Creation de l'appel vers la page de modification du gas
 * @param {*} oxy
 * @param {*} he
 * @param {*} ppo2
 * @param {*} id
 */
function edGas(oxy, he, ppo2, id) {
    myDebug("edGas avec oxy=" + oxy + ", he=" + he + ", ppo2=" + ppo2);
    globalGasEd = { oxy: oxy, he: he, ppo2: ppo2, id: id };
    myDebug("edGas call gotopage config-addGas");
    gotoPage('config-addGas.html', false);
    //Q: Possible de modifier le contenu de la page ?
}

/**
 * note: pas d'id pour la liste globale car on se réserve le droit d'avoir des gaz différents dans les anciennes plongées...
 * 
 * @param {*} objDropDown 
 * @param {*} objTarget 
 * @param {*} oxy 
 * @param {*} he 
 * @param {*} ppo2 
 */
function selectGas(objDropDown, objTarget, oxy, he, ppo2) {
    myDebug("select Gas " + oxy + " he=" + he + ", ppo2=" + ppo2);
    globalGasEd = { oxy: oxy, he: he, ppo2: ppo2 };
    $(objDropDown).hide();
    $(objTarget).html(gasLabel(oxy, he));
    document.querySelector(objDropDown).hideExpansion();
    $(objDropDown).show();
}
