/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

/**
 * Fonction de debug locale qui permet de passer en mode silent en prod
 *
 * @param   {string}  msg  message de debug
 *
 * @return  {[type]}       [return description]
 */
function myDebug(msg) {
    if (globalDevMode) {
        if ("object" === typeof msg) {
            console.log('MVPDEBUG : ' + JSON.stringify(msg));
        }
        else {
            console.log('MVPDEBUG : ' + msg);
        }
    }
}

/**
 * retourne la date au format JJ/MM/YYYY
 *
 * @param   {[type]}  ladate  [ladate description]
 *
 * @return  {[type]}          [return description]
 */
function dateFormatDDMMYYYY(ladate) {
    let formattedDate = new Date(ladate);
    let d = formattedDate.getDate();
    let m = formattedDate.getMonth();
    m += 1;  // JavaScript months are 0-11
    let y = formattedDate.getFullYear();
    return (d + "/" + m + "/" + y);
}

/**
 * retourne la date au format YYYY-MM-JJ
 *
 * @param   {[type]}  ladate  [ladate description]
 *
 * @return  {[type]}          [return description]
 */
function dateFormatYYYYMMDD(ladate) {
    let formattedDate = new Date(ladate);
    let d = formattedDate.getDate();
    let m = formattedDate.getMonth();
    m += 1;  // JavaScript months are 0-11
    let y = formattedDate.getFullYear();
    return (y + "-" + m + "-" + d);
}

/**
 * Retourne un nombre formaté à la française
 *
 * @param   {[type]}  nb  [nb description]
 *
 * @return  {[type]}      [return description]
 */
function nbFR(nb) {
    res = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(nb);
    return res;
}

// var nbFR = new Intl.NumberFormat('fr-FR', {
//     style: 'currency',
//     currency: 'EUR',
// });

//basename d'un fichier
function basename(path) {
    return path.replace(/\\/g, '/').replace(/.*\//, '');
}

//dirname d'un fichier
function dirname(path) {
    return path.replace(/\\/g, '/').replace(/\/[^\/]*$/, '');;
}


/**
 * Fonction basique et pas terrible de comparaison de deux versions ...
 * @param {*} a version a sous la forme 1.2.3
 * @param {*} b version b sous la forme 1.2.4
 * @param {*} taille pour savoir où on stoppe la comparaison, par exemple 1.6.2 et 1.6.4 = 0 si taille = 2 (compare 1.6 et 1.6)
 * @returns -2 si a ou b sont vides ou indéfinis ou null
 *          -1 si b > a
 *           0 si a == b
 *           1 si a > b
 */
function compareVersions(a, b, taille = 3) {
    myDebug("compare version " + a + " et " + b + ";")
    if (a == "" || a === undefined || a == null || b === undefined || b == null || b == "") {
        myDebug(" retour -2");
        return -2;
    }
    var pa = a.split('.');
    var pb = b.split('.');
    for (var i = 0; i < taille; i++) {
        var na = Number(pa[i]);
        var nb = Number(pb[i]);
        if (na > nb) {
            myDebug(" retour 1");
            return 1;
        }
        if (nb > na) {
            myDebug(" retour -1");
            return -1;
        }
        if (!isNaN(na) && isNaN(nb)) {
            myDebug(" retour 1");
            return 1;
        }
        if (isNaN(na) && !isNaN(nb)) {
            myDebug(" retour -1");
            return -1;
        }
    }
    myDebug(" retour 0");
    return 0;
};

//Affiche un "wait" message ... un sablier quoi
function showWait() {
    // myDebug("Appel a showWait");
    $("#sablier").fadeIn(250);
    setTimeout(function () { hideWait() }, 120000);
}

//Cache le sablier
function hideWait() {
    // myDebug("Appel a hideWait");
    setTimeout(function () { $("#sablier").fadeOut(250) }, 800);
}

// Retourne une icone a partir de l'id si on n'en a pas a partir du label
// En bref par ex. pour la "restauration" on a des icones de
//   café, hotdog, gateau, cookie, glace, hamburger, poisson, pizza
// ou une icone générique ...
function faIcone(label, id) {
    let icone = "";
    icone = faIconeLabel(label);
    if (icone == "") {
        icone = faIconeId(id);
    }
    return icone;
}

// Icone à partir du texte
function faIconeLabel(label) {
    if ((typeof label != 'undefined') && (label !== null) && (label != "")) {
        let r = "";
        // myDebug('faIconeLabel : ' + label);
        let tabCorrespondance = {
            "sandwich": "hamburger",
            "hamburger": "hamburger",
            "mcdo": "hamburger",
            "café": "coffee",
            "pizza": "pizza-slice",
            "restaurant": "utensils",
            "resto": "utensils",
            "glace": "ice-cream",
            "poisson": "fish",
            "biscuit": "cookie",
            "salade": "carrot",
            "fruit": "apple-alt",

            "bus": "bus",
            "autobus": "bus",
            "avion": "plane",
            "métro": "subway",
            "tram": "tram",
            "train": "train",
            "tgv": "train",
            "ter": "train",
            "taxi": "taxi",
            "vtc": "shuttle-van",
            "moto": "motorcycle",
            "autoroute": "road",
            "péage": "road",

            "timbres": "mail-bulk",
            "ups": "ups",
            "dhl": "dhl",
            "amazon": "amazon",
            "airbnb": "airbnb",
            "apple": "apple",
            "aws": "aws",
            "bitcoin": "bitcoin",
            "dropbox": "dropbox",
            "ebay": "ebay",
            "facebook": "facebook",
            "fedex": "fedex",
            "google": "google",
            "itunes": "itunes-note",
            "linux": "linux",
            "microsoft": "microsoft",
            "paypal": "paypal",
            "stripe": "stripe-s",
            "uber": "uber",
            "viadeo": "viadeo",

        };

        let keys = Object.keys(tabCorrespondance);
        for (let key of keys) {
            let search = new RegExp(key, 'iu');
            // console.log("On cherche " + search + " dans " + label);
            if (label.match(search)) {
                // console.log("On a trouvé pour " + key + " -> " + tabCorrespondance[key]);
                r = tabCorrespondance[key];
                break;
            }
        }

        // myDebug('faIconeLabel : ' + r);
        if (r != "") {
            return "<i class=\"fa fa-" + r + "\"></i> &nbsp; ";
        }
        else {
            return "";
        }
    }
    else {
        return "";
    }
}

// Icone à partir de l'id type de frais
function faIconeId(id) {
    // myDebug('faIcone : ' + id);

    let r = "question";
    switch (id) {
        case 1:
            r = "utensils";
            break;
        case 2:
            r = "parking";
            break;
        case 3:
            r = "hotel";
            break;
        case 4:
            r = "space-shuttle";
            break;
        case 5:
            r = "taxi";
            break;
        case 6:
            r = "gas-pump";
            break;
        case 7:
            r = "scroll";
            break;
        case 8:
            r = "car-side";
            break;
    }
    // myDebug('faIcone : ' + r);
    return "<i class=\"fa fa-" + r + "\"></i> &nbsp; ";
}

//retourne un tableau sans doublons
function myUniqueSort(array) {
    return $.grep(array, function (el, index) {
        return index === $.inArray(el, array);
    });
}

//Transforme une URI locale en Blob
function dataURItoBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var mimeString = dataURI.split(",")[0].split(":")[1].split(";")[0];
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: mimeString });
}

//source : https://stackoverflow.com/questions/21227078/convert-base64-to-image-in-javascript-jquery
function base64toBlob(base64Data, contentType) {
    contentType = contentType || '';
    var sliceSize = 1024;
    var byteCharacters = atob(base64Data);
    var bytesLength = byteCharacters.length;
    var slicesCount = Math.ceil(bytesLength / sliceSize);
    var byteArrays = new Array(slicesCount);

    for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
        var begin = sliceIndex * sliceSize;
        var end = Math.min(begin + sliceSize, bytesLength);

        var bytes = new Array(end - begin);
        for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
            bytes[i] = byteCharacters[offset].charCodeAt(0);
        }
        byteArrays[sliceIndex] = new Uint8Array(bytes);
    }
    return new Blob(byteArrays, { type: contentType });
}

function getFileContentAsBase64(path, callback) {
    myDebug('getFileContentAsBase64 : ' + path);
    filePath = path;
    if (!path.startsWith("file://")) {
        filePath = 'file://' + path;
    }
    window.resolveLocalFileSystemURL(filePath, gotFile, fail);

    function fail(e) {
        myDebug('getFileContentAsBase64 ERROR');
    }

    function gotFile(fileEntry) {
        fileEntry.file(function (file) {
            var reader = new FileReader();
            reader.onloadend = function (e) {
                var content = this.result;
                myDebug('getFileContentAsBase64 base64 read ok :' + content.length);
                callback(content);
            };
            // The most important point, use the readAsDatURL Method from the file plugin
            reader.readAsDataURL(file);
        });
    }
}

function ProgressBar(ele) {
    this.thisEle = $(ele);
    this.fileAdded = function () {
        myDebug("ProgressBar : fileAdded");
        (this.thisEle).removeClass('hide').find('.progress-bar').css('width', '0%');
    },
        this.uploading = function (progress) {
            myDebug("ProgressBar : progress..." + progress);
            // (this.thisEle).find('.progress-bar').attr('style', "width:" + progress + '%');
            $('.progress-bar').attr('style', 'width:' + progress + '%');
        },
        this.finish = function () {
            myDebug("ProgressBar : finish");
            gotoPage("messageDone.html");
        }
}

function encodeHTMLEntities(text) {
    return $("<textarea/>").text(text).html();
}

function decodeHTMLEntities(text) {
    return $("<textarea/>").html(text).text();
}

function fixedEncodeURIComponent(str) {
    return encodeURIComponent(str).replace(/[!'()*]/g, escape);
}

/**
 * Delete file (fileName) in directory (dirName)
 * @param {string} dirName
 * @param {string} fileName
 */
function deleteFile(dirName, fileName) {
    myDebug('deleteFile : dir=' + dirName + " file=" + fileName);
    window.resolveLocalFileSystemURL(dirName, function (dir) {
        dir.getFile(fileName, { create: false }, function (fileEntry) {
            fileEntry.remove(function (file) {
                myDebug("File " + fileName + " removed [ok]");
            }, function (error) {
                myDebug("Error deleting file " + fileName + " " + error.code);
            }, function () {
                myDebug("Error deleting file " + fileName + " does not exist");
                alert("file does not exist");
            });
        });
    });
}

/**
 * @returns hash for a string (poor man uuid)
 */
String.prototype.hashCode = function () {
    var hash = 0,
        i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
}



var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  };
  
  function escapeHtml (string) {
    return String(string).replace(/[&<>"'`=\/]/g, function fromEntityMap (s) {
      return entityMap[s];
    });
  }