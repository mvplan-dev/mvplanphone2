/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

var databaseEngine = 'IndexedDB';
var databaseName = 'mvplanDB';
var databaseVersion = 1;
var globalIndexedDB = null;               // Le connecteur vers la base de données IndexedDB

