/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

ons.ready(function () {
  // on capture le bouton back de l'OS
  ons.enableDeviceBackButtonHandler();

  // Set a new handler
  ons.setDefaultDeviceBackButtonListener(function (event) {
    fn.back();
  });

  var path = window.location.pathname;
  var page = path.split("/").pop();
  // if (page != "index.html") {
  //   if (globalIndexedDB == null && databaseEngine == 'IndexedDB') {
  //     var openRequest = window.indexedDB.open(databaseName, databaseVersion);
  //     openRequest.onerror = function (event) {
  //       myDebug("global db type IndexedDB open err (ndf-head)" + openRequest.errorCode);
  //     };
  //     openRequest.onsuccess = function (event) {
  //       // Database is open and initialized - we're good to proceed.
  //       globalIndexedDB = openRequest.result;
  //       myDebug("global db type IndexedDB open ok (ndf-head)");
  //     }
  //   }
  // }
});



/* View in fullscreen */
function openFullscreen() {
  var elem = document.documentElement;
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.webkitRequestFullscreen) { /* Safari */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE11 */
    elem.msRequestFullscreen();
  }
}
