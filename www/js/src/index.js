/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

var app = {
  // Application Constructor
  initialize: function () {
    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
  },

  // deviceready Event Handler
  //
  // Bind any cordova events here. Common events are:
  // 'pause', 'resume', etc.
  onDeviceReady: function () {
    // if ( device.platform === "iOS" && parseInt( device.version ) === 9 ) {
    //   $.mobile.hashListeningEnabled = false;
    // }

    //device.overrideBackButton();
    document.addEventListener("backKeyDown", function () {
      // myDebug('onBackKeyDown');
      fn.back();
    }, false);

    this.receivedEvent('deviceready');
  },

  // Update DOM on a Received Event
  receivedEvent: function (id) {
    var parentElement = document.getElementById(id);
    if (parentElement !== null) {
      var listeningElement = parentElement.querySelector('.listening');
      var receivedElement = parentElement.querySelector('.received');

      listeningElement.setAttribute('style', 'display:unset;');
      receivedElement.setAttribute('style', 'display:block;');

      // myDebug('Received Event: ' + id);
    }
  }
};

app.initialize();

//La date du jour par défaut
Date.prototype.toDateInputValue = (function () {
  var local = new Date(this);
  local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
  return local.toJSON().slice(0, 10);
});

