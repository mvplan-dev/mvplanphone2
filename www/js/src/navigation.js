/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

//Demande si on quitte l'appli ...
function askQuitOrGoto(destination) {
    // myDebug('askQuitOrMenu, titre en cours = ' + globalCurrentTitle + " / " + globalCurrentPage);
    // if (globalCurrentTitle == "Mvplan") {
    if (device !== undefined) {
        if (device.platform == "iOS") {
            gotoPage(destination, false);
        }
        else {
            let optionsNotif = { buttonLabels: ["Oui", "Non"], cancelable: true, title: "Confirmation" };
            ons.notification.confirm("Session fermée.\nVoulez-vous quitter l'application ?", optionsNotif)
                .then(function (index) {
                    if (index === 0) {
                        if (device.platform == "Android") {
                            navigator.app.exitApp();
                        }
                        else {
                            // globalCurrentTitle = "Mvplan";
                            gotoPage(destination, false);
                        }
                    }
                    else {
                        // globalCurrentTitle = "Mvplan";
                        gotoPage(destination, false);
                    }
                });
        }
    }
    else {
        myDebug("device is undef !");
    }
    // else {
    //     globalCurrentTitle = "Mvplan";
    //     gotoPage(destination, false);
    // }
}

//On garde les 20 dernieres étapes de l'historique ...
// -----------------------------
function historique(page) {
    myDebug('historique : ' + page + " > " + globalCurrentPage);
    if (page != globalCurrentPage) {
        globalCurrentPage = page;
        // myDebug('on ajoute à history : ' + page);
        var listeContent = JSON.parse(localGetData("history"));
        // myDebug('history list avant la magie : ' + listeContent);
        if (Array.isArray(listeContent)) {
            //On essaye d'eviter une boucle infinie
            var prevpage = listeContent.pop();
            if (prevpage != page) {
                listeContent.push(prevpage);
            }
            listeContent.push(page);
            //On ne garde que les 20 dernieres pages
            listeContent.splice(0, listeContent.length - 20);
        }
        else {
            listeContent = new Array(page);
        }
        // myDebug('history list apres la magie : ' + listeContent);
        localStoreData("history", JSON.stringify(listeContent));
    }
}

//Ferme la page en cours
function closePage() {
    myDebug("closePage avec  " + JSON.stringify(globalTabPages));
    if (globalMyNavigator !== undefined) {
        myDebug("closePage 2 on est actuellement sur : " + JSON.stringify(globalMyNavigator.topPage.data.name));
        myDebug("closePage 2 on est actuellement sur (variable): " + globalCurrentPage);
        // globalCurrentPage = globalMyNavigator.topPage.name;
        let opt = { animation: 'fade' };
        globalMyNavigator.popPage(opt).then(myDebug("closePage 3 on est actuellement sur : " + JSON.stringify(globalMyNavigator.topPage.data.name)));
    }
    return true;
}

function gotoPage(destination, saveHistory = true) {
    myDebug("gotoPage 1 (" + destination + ") historique (" + saveHistory + ")");

    // //Si on est déjà sur la page on ne fait "rien"
    if (globalMyNavigator.topPage != null)
        if (globalMyNavigator.topPage.data.name == destination) {
            return;
        }

    if (globalMyNavigator !== undefined) {
        globalCurrentPage = destination;
        myDebug("gotoPage : on change globalCurrentPage pour avoir maintenant : " + globalCurrentPage);
        //Si on arrive sur le menu on zappe toutes les autres pages possibles de la stack
        if (destination == "menu.html" || destination == "historique.html") {
            globalMyNavigator.resetToPage(destination, { data: { title: 'avenir', name: destination }});
        }
        else {
            globalMyNavigator.bringPageTop(destination, { data: { title: 'avenir', name: destination } });
        }
    }

    let onsRightMenu = document.getElementById('rightmenu');
    if (onsRightMenu != undefined)
        onsRightMenu.close();

    // requestKeepAlive();
    
    myDebug("gotoPage 2 call updatePage");
    updatePage(destination);

    return true;
}


function cleanHistorique() {
    myDebug('call cleanHistorique');
    listeContent = new Array('');
    localStoreData("history", JSON.stringify(listeContent));
}

//Fermeture de session
function closeSession() {
    logoutOnMvplan();
    localStoreData("api_token", null);
    localStoreData("name_server", null);
    askQuitOrGoto("login.html");
}

//Tout le code pour les pages
function updatePage(lapage) {
    var objets = "";
  
    myDebug("updatePage : " + lapage);
  
    //Dans le cas où on est sur l'index / accueil / lancement initial  on ne fait rien
    if (lapage == "loadingPage" || lapage == "ONSSplitter") {
    //   return;
    }
   
    if (lapage == 'about.html') {
      // setPageTitle("À propos");
      feedMvplanInfos();
      feedAproposVersion();
      feedAproposCustom();
      //apres des feed il faut refaire une traduction
    //   return;
    }

    if (lapage == 'gas.html') {
        myDebug("Call updatePage for gas.html ...");

        // var pullHook = document.getElementById('pull-hookGas');
        // pullHook.addEventListener('changestate', function (event) {
        //   var message = '';
  
        //   switch (event.state) {
        //     case 'initial':
        //       message = 'Pull to refresh';
        //       break;
        //     case 'preaction':
        //       feedGasConfig("#feedGas", "gas");
        //       //apres des feed il faut refaire une traduction
        //       message = 'Release';
        //       break;
        //     case 'action':
        //       message = 'Loading...';
        //       break;
        //   }
  
        //   pullHook.innerHTML = message;
        // });
  
        // pullHook.onAction = function (done) {
        //   setTimeout(done, 1000);
        // };

        //apres des feed il faut refaire une traduction
        // translateUI();
  
        // setPageTitle("Configuration");
        //   return;
        myDebug("end Call updatePage for gas.html ...");

    }
  
    if (lapage == 'config-addGas.html') {
      }
  
    if (lapage == 'tanks.html') {
    }
    if (lapage == 'config-addTank.html') {
    }
  
  
    if (lapage == 'dives.html') {
  
    }
    if (lapage == 'config-addDive.html') {
  
    }
  
    // ------------- login
    // if (lapage == 'ONSlogin') {
    //   myDebug("ons show login.html ...");
  
    //   // myDebug("SELECT * FROM config ERROR");
    //   if (localGetData("api_server") !== null) {
    //     //Au lancement on regarde si on a pas déjà ces infos de la dernière fois ...
    //     // switchServer('#server', localGetData("name_server"), localGetData("api_server"));
    //     //Et on essaye de reprendre la session ! sauf si on a plusieurs comptes sur l'appli
    //     //en ce cas on affiche toujours le user switcher au lancement !
    //     myDebug("Login with API token...");
    //     let a = localGetData("accounts");
    //     if (a !== null && a !== "") {
    //       accounts = JSON.parse(a);
    //       if (accounts.length == 1) {
    //         loginWithAPIToken();
    //       }
    //     }
    //   }
  
    //   $(".toggle-password").click(function () {
    //     myDebug("Clic sur l'affichage du mot de passe ...");
  
    //     $(this).toggleClass("fa-eye fa-eye-slash");
    //     var input = $($(this).attr("toggle"));
    //     if (input.attr("type") == "password") {
    //       input.attr("type", "text");
    //     } else {
    //       input.attr("type", "password");
    //     }
    //   });
  
    //   // myDebug("carouselLogin call 1");
    //   // carouselLogin();
    //   return;
    // }
  
    // ------------- menu
    if (lapage == 'menu.html') {
    }
  
    // ------------- upload ok
    if (lapage == 'ONSMessageDone') {
      // $('#titreMvplan').replaceWith('<div id="titreMvplan" class="left" onclick="gotoPage(\'menu.html\');">Mvplan</div>');
      // $('#menuHautDroite').replaceWith('<div id="menuHautDroite" class="right"><ons-toolbar-button onclick="fn.open()"><ons-icon icon="md-menu"></ons-icon></ons-toolbar-button></div>');
      // globalCurrentTitle = 'UploadOK';
    //   return;
    }
  
    // ------------- upload err
    if (lapage == 'ONSMessageError') {
      // $('#titreMvplan').replaceWith('<div id="titreMvplan" class="left" onclick="gotoPage(\'menu.html\');">Mvplan</div>');
      // $('#menuHautDroite').replaceWith('<div id="menuHautDroite" class="right"><ons-toolbar-button onclick="fn.open()"><ons-icon icon="md-menu"></ons-icon></ons-toolbar-button></div>');
      // globalCurrentTitle = 'UploadErr';
    //   return;
    }
  
    // ------------- upload in progress
    // if (lapage == 'ONSMessageUploadProgress') {
    //   $('#titreMvplan').replaceWith('<div id="titreMvplan" class="left" onclick="gotoPage(\'menu.html\');">Téléversement...</div>');
    //   // $('#menuHautDroite').replaceWith('<div id="menuHautDroite" class="right"><ons-toolbar-button onclick="fn.open()"><ons-icon icon="md-menu"></ons-icon></ons-toolbar-button></div>');
    //   // globalCurrentTitle = 'TransfertInProgress';
    //   return;
    // }
  
    // ----------- Code commun
    //On complète automatiquement le champ
    // feedObjets(objets);
  
    //Et on initialise le formulaire
    initialiseForm();
  
    //Pour eviter d'avoir des "envois" de formulaire quand on appuie sur le bouton
    //suivant du keypad/numpad
    $('form input').on('keyup keypress', function (e) {
      var keyCode = e.keyCode || e.which;
      myDebug("keypressed : " + keyCode);
      if (keyCode === 13) {
        e.preventDefault();
        return false;
      }
    });
  }
  