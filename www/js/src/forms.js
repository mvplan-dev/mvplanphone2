/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

// -----------------------------
// Analyseur de formulaire générique ... tant qu'on a pas trouvé mieux
//retourne vrai si le check est ok
function genericCheckForm(leform = null) {
  myDebug("genericCheckForm " + leform);
  if (leform == null) {
    myDebug("genericCheckForm leform null");
    return false;
  }

  let probleme = "";
  // myDebug("genericCheckForm");
  //Les deux tableaux doivent etre synchro, en haut le code de l'objet en bas le message à afficher
  fieldsToValidate = ['nom', 'arrivee', 'depart', 'distance', 'energievalue', 'label', 'ladate', 'email', 'password', 'puissancevalue', 'immat', 'ttc', 'typeFrais', 'typevalue', 'photoFacturette'];
  fieldsTexte = ["Nom", "Arrivée", "Départ", "Distance", "Type de carburant", "Objet", "La date", "Courrier électronique", "Mot de passe", "Puissance du véhicule", "Plaque d'immatriculation", "Montant TTC", "Type de frais", "Type", "Photo de la facturette"];

  //En mode developpeur la photo n'est pas indispensable
  if (globalDevMode) {
    fieldsToValidate.splice(-1, 1);
    fieldsTexte.splice(-1, 1);
  }

  let objForm = document.getElementById(leform);
  if (objForm == null || objForm == undefined) {
    myDebug("genericCheckForm objForm null");
    return false;
  }

  //Si le compte utilisateur est configuré sur le serveur pour etre en mode simple on exige alors que la photo ...
  myDebug("mode d'utilisation pour cette personne : " + localGetData('mode_simple'));
  if (localGetData('mode_simple') == 1) {
    fieldsToValidate = ['photoFacturette'];
    fieldsTexte = ["Photo de la facturette"];
  }
  for (let i = 0; i < fieldsToValidate.length; i++) {
    let field = fieldsToValidate[i];
    //Si il existe mais qu'il est vide alors c'est un pb
    myDebug("on cherche a valider le champ : " + field);

    if (objForm.elements[field] !== undefined) {
      myDebug("Cet element existe, " + field + "(" + i + "), de type " + objForm.elements[field].type + ", quelle est sa valeur ..." + objForm.elements[field].value);

      if (!objForm.elements[field].value) {
        $('#' + field).attr('style', "border-radius: 5px; border:#FF0000 2px solid;");
        probleme += "<ul>" + fieldsTexte[i] + "</ul>";
      }
      // fix #437 : evite de saisir des montants négatifs
      else if (objForm.elements[field].type == "number" && objForm.elements[field].value < 0) {
        $('#' + field).attr('style', "border-radius: 5px; border:#FF0000 2px solid;");
        probleme += "<ul>Montant négatif interdit</ul>";
      }
      else {
        $('#' + field).attr('style', "");
      }
    }
    else {
      myDebug("le champ suivant n'existe pas : " + field);
    }
  }
  if (probleme != "") {
    ons.notification.alert("Certaines informations sont manquantes ou incorrectes :<ul> " + probleme + "</ul>", { title: "Attention" }).then(function () {
      return false;
    });
    // myDebug("genericCheckForm return false " + probleme);
  }
  else {
    myDebug("genericCheckForm return true");
    return true;
  }
}


//initialise le formulaire avec des valeurs par défaut ...
function initialiseForm() {
  myDebug('initialiseForm ... ');

  //Si on a un objet "ladate" on essaye de lui attribuer par défaut la date du jour
  if (document.getElementById('ladate')) {
    let today = new Date();
    $('#ladate').val(today.toDateInputValue());
    // myDebug('date : ');
    // myDebug(today.toDateInputValue());
    // myDebug("Résultat de l'affectation : " + document.getElementById('ladate').value);
  }

  if (document.getElementById('listeGas')) {
    // myDebug('Depuis initialiseForm, appel listeGas ...');
  }

  //Sur iPhone on transforme les champs de type input=number pour avoir le clavier special
  //issu du plugin cordova-plugin-decimal-keyboard
  if (device !== undefined) {
    if (device.platform == "iOS") {
      $('input[type="number"]').attr('pattern', '[0-9]*');
      $('input[type="number"]').attr('decimal', 'true');
      $('input[type="number"]').prop('type', 'text');
    }
  }
  else {
    myDebug("device is undef !");
  }
  translateUI();

  myDebug('initialiseForm end');
}

/**
* Purpose: blink a page element
* Preconditions: the element you want to apply the blink to,
    the number of times to blink the element (or -1 for infinite times),
    the speed of the blink
**/
function blinkElement(elem, times, speed) {
  if (times > 0 || times < 0) {
    if ($(elem).hasClass("blink"))
      $(elem).removeClass("blink");
    else
      $(elem).addClass("blink");
  }

  clearTimeout(function () { blinkElement(elem, times, speed); });

  if (times > 0 || times < 0) {
    setTimeout(function () { blinkElement(elem, times, speed); }, speed);
    times -= .5;
  }
}
