/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019-2021 - GNU AGPLv3
*/

/**
 * Ajout d'une entrée dans le stockage local
 *
 * @param   {[type]}  liste  [liste description]
 * @param   {[type]}  entry  [entry description]
 *
 * @return  {[type]}         [return description]
 */
function addLocalStorageEntry(liste, entry) {
  if ((typeof liste != 'undefined') && (liste !== null) && (liste != "")) {
    if ((typeof entry != 'undefined') && (entry !== null) && (entry != "")) {
      // myDebug('Appel de la sauvegarde de la liste des objets pour ...' + liste);
      let listeContent = JSON.parse(localGetData(liste));
      if (Array.isArray(listeContent)) {
        listeContent.push(entry);
        listeContent = myUniqueSort(listeContent);
      }
      else {
        listeContent = new Array(entry);
      }
      localStoreData(liste, JSON.stringify(listeContent));
    }
  }
}


/**
 * Ajout ou remplace une donnée dans la base de données
 * (stockage persistant)
 *
 * @param   {[type]}  lakey    [lakey description]
 * @param   {[type]}  lavalue  [lavalue description]
 *
 * @return  {[type]}           [return description]
 */
function localStoreData(lakey, lavalue) {
  myDebug('localStoreData : ' + lakey + ' et ' + lavalue);
  localStorage.setItem(lakey, lavalue);

  //Si c'est un bloc on garde la date de dernière mise à jour locale pour
  //pouvoir faire une sync serveur si nécessaire
  // if (lakey == "tanks") {
  //   let ladate = new Date();
  //   let lastUpdate = ladate.toISOString();
  //   localStorage.setItem("tanksLastUpdate", lastUpdate);
  // }

  // myDebug('localStoreData : ' + key + ' et ' + value);
  //Pas la peine de stocker ça en SQL
  if (lakey == 'history') {
    return;
  }

  //shortcut
  if (databaseEngine != 'IndexedDB') {
    return;
  }
  if (globalIndexedDB == null) {
    myDebug('  localStoreData : bdd non connectée, return');
    //On est dans les appels avant que la connexion bdd ne soit active
    return;
    // // myDebug("SQL : On essaye d'ouvrir la base de données ...");
    // // if (databaseEngine == 'IndexedDB') {
    // let openRequest = window.indexedDB.open(databaseName, databaseVersion);
    // openRequest.onerror = function (event) {
    //   myDebug(openRequest.errorCode);
    //   myDebug("global db type IndexedDB re-open error");
    // };
    // openRequest.onsuccess = function (event) {
    //   // Database is open and initialized - we're good to proceed.
    //   globalIndexedDB = openRequest.result;
    //   myDebug("global db type IndexedDB re-open ok");
    //   localStoreDataNext(lakey, lavalue);
    // };
  }
  else {
    localStoreDataNext(lakey, lavalue);
  }
}

function localStoreDataNext(lakey, lavalue) {
  myDebug('  localStoreDataNext : ' + lakey + ' et ' + lavalue);
  if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
    // if (databaseEngine == 'IndexedDB') {
    myDebug("  localStoreData put data asked for " + lakey + ":" + lavalue);

    if ((typeof lavalue != 'undefined') && (lavalue !== null) && (lavalue != "")) {
      myDebug("  localStoreData on a une valeur non nulle a stocker : " + lavalue);
    }
    else {
      myDebug("  localStoreData on a une valeur vide ou nulle a stocker");
      lavalue = "none";
    }

    //Tout sur une ligne pour eviter le pb des transactions qui se chevauchent...
    let transaction = globalIndexedDB.transaction(['mvplanconfig'], "readwrite").objectStore('mvplanconfig').put({ key: lakey, data: lavalue });

    // transaction.oncomplete = function (event) {
    //   myDebug("  localStoreData global db transaction.oncomplete ok ( " + lakey + ":" + lavalue + ")");

    //   let objectStore = transaction.objectStore('mvplanconfig');

    //   objectStore.oncomplete = function (event) {
    //     let request = objectStore.put({ key: lakey, data: lavalue });
    //     request.oncomplete = function (event) {
    //       myDebug("  localStoreData put oncomplete ok pour " + lakey + ":" + lavalue);
    //     };
    //     request.onsuccess = function (event) {
    //       myDebug("  localStoreData put onsuccess ok pour " + lakey + ":" + lavalue);
    //     };
    //     request.onerror = function (event) {
    //       myDebug("  localStoreData put err pour " + lakey + " : " + lavalue);
    //     };
    //   }

    //   objectStore.onerror = function (event) {
    //     myDebug("  localStoreData objectStore.onerror : " + event.error);
    //   }

    // }

    // transaction.onerror = function (event) {
    //   myDebug("  localStoreData global db transaction.onerror : " + event.error);
    // };
  }
  else {
    myDebug("  ERR: Accès à globalIndexedDB impossible (1) pour stocker " + lakey + " val " + lavalue + " *** ");
  }
}

/**
 * Récupère une donnée locale
 *
 * @param   {[type]}  key  [key description]
 *
 * @return  {[type]}       [return description]
 */
function localGetData(key, defaultValue = null) {
  if (
    (typeof localStorage.getItem(key) != 'undefined')
    && (localStorage.getItem(key) !== null)
    && (localStorage.getItem(key) != null)
    && (localStorage.getItem(key) != 'null')
    && (localStorage.getItem(key) != "")
    && (localStorage.getItem(key) != undefined)
    && (localStorage.getItem(key) != 'undefined')
  ) {
    let v = localStorage.getItem(key);
    myDebug('localGetData : ' + key + ' -> ' + v);
    if (v == "none") {
      v = "";
    }
    return v;
  }
  myDebug("localGetData: retour de la valeur par defaut " + defaultValue + " pour " + key)
  return defaultValue;
}


function localStoreLDFtoSync(urlIN, jsonDataIN, localFileNameIN) {
  return new Promise((resolve, reject) => {
    myDebug("localStoreLDFtoSync :" + urlIN + " " + jsonDataIN + " " + localFileNameIN);
    let d = new Date();
    let ladateIN = d.toISOString();
    let lavalue = { url: urlIN, jsonData: jsonDataIN, localFileName: localFileNameIN, ladate: ladateIN };
    let id = -1;

    if (globalIndexedDB == null && databaseEngine == 'IndexedDB') {
      myDebug("  localStoreLDFtoSync On essaye de ré-ouvrir la base de données ...");
      // if (databaseEngine == 'IndexedDB') {
      let openRequest = window.indexedDB.open(databaseName, databaseVersion);
      openRequest.onerror = function (event) {
        myDebug(openRequest.errorCode);
        myDebug("  localStoreLDFtoSync db type IndexedDB re-open error");
      };
      openRequest.onsuccess = function (event) {
        // Database is open and initialized - we're good to proceed.
        globalIndexedDB = event.target.result;
        myDebug("  localStoreLDFtoSync db type IndexedDB re-open ok");
      };
    }

    if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
      myDebug("  localStoreLDFtoSync db type IndexedDB put data asked pour " + JSON.stringify(lavalue));
      let transaction = globalIndexedDB.transaction(["ldfsToSync"], "readwrite");
      let objectStore = transaction.objectStore("ldfsToSync");
      let request = objectStore.put(lavalue);

      request.onsuccess = function (event) {
        myDebug("  localStoreLDFtoSync put ok, id = " + event.target.result + " pour " + urlIN);
        id = event.target.result;
        globalLastInsertId = id;
        myDebug("  localStoreLDFtoSync globalLastInsertId = " + globalLastInsertId);
        resolve(id);
      };

      request.onerror = function (event) {
        myDebug("  localStoreLDFtoSync put err pour " + urlIN);
        reject(null);
      };
    }
    else {
      myDebug("  localStoreLDFtoSync ERR: Accès à globalIndexedDB (2) impossible pour stocker " + urlIN);
      reject(null);
    }
  });
}


function localDeleteLDFtoSync(idIN) {
  //dev time
  myDebug("localDeleteLDFtoSync :" + idIN);

  if (globalIndexedDB == null && databaseEngine == 'IndexedDB') {
    myDebug("globalIndexedDB : On essaye d'ouvrir la base de données ...");
    // if (databaseEngine == 'IndexedDB') {
    let openRequest = window.indexedDB.open(databaseName, databaseVersion);
    openRequest.onerror = function (event) {
      myDebug(openRequest.errorCode);
      myDebug("  localDeleteLDFtoSync db type IndexedDB re-open error");
    };
    openRequest.onsuccess = function (event) {
      // Database is open and initialized - we're good to proceed.
      globalIndexedDB = event.target.result;
      myDebug("  localDeleteLDFtoSync db type IndexedDB re-open ok");
    };
  }

  if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
    myDebug("  localDeleteLDFtoSync db type IndexedDB put data asked ...");
    let transaction = globalIndexedDB.transaction(["ldfsToSync"], "readwrite");
    let objectStore = transaction.objectStore("ldfsToSync");
    let request = objectStore.delete(idIN);

    request.onsuccess = function (event) {
      myDebug("  localDeleteLDFtoSync delete ok");
    };

    request.onerror = function (event) {
      myDebug("  localDeleteLDFtoSync put err pour " + urlIN);
    };
  }
  else {
    myDebug("  localDeleteLDFtoSync ERR: Accès à globalIndexedDB impossible (3) pour stocker " + urlIN);
  }
}

function localUpdateLDFfileName(idIN, newFileName) {
  if (idIN > 0) {
    if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
      myDebug("localUpdateLDFfileName id=" + idIN);

      let objectStore = globalIndexedDB.transaction("ldfsToSync").objectStore("ldfsToSync", "readwrite");
      objectStore.get(idIN).onsuccess = function (event) {
        myDebug("  localUpdateLDFfileName get ok " + JSON.stringify(event.target.result));

        let v = event.target.result;
        //Mise a jour uniquement si non vide
        if (v !== null && v != "") {
          v.localFileName = newFileName;

          // Update the notified value in the object to "yes"
          v.notified = "yes";

          // Create another request that inserts the item back into the database
          let vUpdate = objectStore.put(v);
          myDebug("  localUpdateLDFfileName originated this request is " + vUpdate.transaction);

          vUpdate.onsuccess = () => {
            myDebug("  localUpdateLDFfileName rename filename ok");
          };

          vUpdate.onerror = () => {
            myDebug("  localUpdateLDFfileName rename filename error");
          };

        }
      }
      objectStore.get(idToSync).onerror = function (event) {
        myDebug("  localUpdateLDFfileName Error" + JSON.stringify(event));
      }
    }
  }
  else {
    myDebug("  localUpdateLDFfileName Error pas d'id passé en paramètre !");
  }
}




/**
 * Supprime une entrée de la base
 *
 * @param   {[type]}  lakey    [lakey description]
 *
 * @return  {[type]}           [return description]
 */
function localRemoveData(lakey) {
  myDebug('localRemoveData : ' + lakey);
  localStorage.removeItem(keyName);

  // myDebug('localStoreData : ' + key + ' et ' + value);
  //Pas la peine de stocker ça en SQL

  //shortcut
  if (databaseEngine != 'IndexedDB') {
    return;
  }
  if (globalIndexedDB == null) {
    myDebug('  localStoreData : bdd non connectée, return');
    return;
  }
  else {
    localRemoveDataNext(lakey);
  }
}

function localRemoveDataNext(lakey) {
  myDebug('  localRemoveDataNext : ' + lakey);
  if (globalIndexedDB !== null && databaseEngine == 'IndexedDB') {
    // if (databaseEngine == 'IndexedDB') {
    myDebug("  localRemoveDataNext remove data for " + lakey);

    //Tout sur une ligne pour eviter le pb des transactions qui se chevauchent...
    let transaction = globalIndexedDB.transaction(["mvplanconfig"], "readwrite").objectStore("mvplanconfig").delete(lakey);
  }
  else {
    myDebug("  ERR: Accès à globalIndexedDB impossible (1) pour supprimer " + lakey);
  }
}
