/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

// Wait for Cordova to connect with the device
//
document.addEventListener("deviceready", NDFonDeviceReady, false);

// Cordova is ready to be used!
//
function NDFonDeviceReady() {
  globalDive = require("/js/scuba-dive.js");
  myDebug("NDFonDeviceReady de ndf.js, globalDive is set");
}

function feedObjets(objets) {
  if ((typeof objets != 'undefined') && (objets !== null) && (objets != "")) {
    for (var i = 0; i < objets.length && i < 15; i++) {
      $('#listeObjets').append('<div class=\"expandable-content  list-item__expandable-content\"><a href="#" onclick="selectLabel(\'' + objets[i] + '\');">' + objets[i] + '</a></div>');
    }
  }
}

function selectLabel(objet) {
  if ((typeof objet != 'undefined') && (objet !== null) && (objet != "")) {
    $('#listeObjets').hide();
    $('#label').val(objet);
    $('#label').focus();
    document.querySelector('#listeObjets').hideExpansion();
    $('#listeObjets').show();
  }
}

//Permet de choisir le texte / titre de la page depuis la page elle même et non dans l'appel de gotoPage
function setPageTitle(titre) {
  myDebug("setPageTitle : " + titre);
  globalCurrentTitle = titre;
  $('#titrePage').html(trans(titre));
}

//sera appellee lorsque la page sera bien chargee pour completer automatiquement le formulaire
//avec les donnees a modifier
function completePageAfterGoto(dataLDF) {
  myDebug("completePageAfterGoto ... : " + JSON.stringify(dataLDF));
}

ons.ready(function () {
  myDebug("ONS Ready, ready in ndf.js");

  $('#label').keyup(function () {
    $('#listeObjets').show();
  });

  // -----------------------------
  // gestion du splitter ...
  let onsRightMenu = document.getElementById('rightmenu');
  if (onsRightMenu != undefined) {
    onsRightMenu.load("parts/splitter.html");
    onsRightMenu.close();
  }

  // gestion du tab bottom
  let onsBottomTab = document.getElementById('bottomTab');
  if (onsBottomTab != undefined) {
    onsBottomTab.load("parts/toolbar.html");
    onsBottomTab.close();
  }

  window.fn = {};

  // =================== migration vers le navigator
  //Ne doit être utilisé qu'une seule fois dans index.html
  //tous les autres appels devraient être gotoPage
  window.fn.load = function (page, data) {
    var content = document.getElementById('globalMyNavigator');
    var onsRightMenu = document.getElementById('rightmenu');
    content.pushPage(page, data)
      .then(onsRightMenu.close.bind(onsRightMenu));
  };

  window.fn.pop = function () {
    var content = document.getElementById('globalMyNavigator');
    content.popPage();
  };

  window.fn.open = function () {
    myDebug("fn.open");

    let onsRightMenu = document.getElementById('rightmenu');
    if (onsRightMenu != undefined)
      onsRightMenu.open();
    else
      myDebug("fn.open : UNDEFINED");
  };
});

