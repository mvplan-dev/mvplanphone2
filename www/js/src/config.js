/*
 * (c) Éric Seigne <eric@videosub.fr> - 2019-2023 - GNU AGPLv3
*/
// **********************************************************************
//Ne pas toucher, mis à jour automatiquement à partir du config.xml
var globalAppVersion = "2.0.0";         // mis à jour automatiquement à partir du fichier config.xml lors du build android
// **********************************************************************


var globalDevMode = true;               // on est en mode dev ou prod ? par defaut oui pour la page d'index
                                        // note en mode dev la photo de la facturette n'est pas obligatoire
var globalDive = undefined;             // dive object

var globalDiveEd = null;                // la plongée qu'on est en train d'éditer
var globalDiveSegmentEd = null;         // le segment de plongée qu'on est en train d'éditer
var globalDiveFreshWater = true;        // by default salt water
var globalGasEd = null;                 // pour savoir si on est en mode édition d'un gaz ou ajout d'un gaz (null)

var globalNetworkResponse = Date.now(); // Date a laquelle on a reçu une réponse du serveur
var globalTabPages = [];                // Vu qu'on utilise plus vraiment le navigator du fait des tab il faut avoir un moyen de gérer l'historique de mouvement pour revenir dans l'historique...
var globalCurrentPage = "";             // Page courante pour gérer le retour précédent
var globalCurrentTitle = "";            // Titre de la page courante
var globalTenMinuts = 60 * 10 * 1000;   // 10 minutes pour l'implementation du keepalive (avant de basculer en full API je pense)
var globalCompatibleWithAPIVersion = 4; // On incrémentera la version de l'API compatible quand on modifie des choses importantes sur le serveur
var globalMvplanDomain = "";            // Pour basculer sur un serveur auto-hebergé
var globalLastInsertId = "";            // Avant d'envoyer les données au serveur on les stocke en cache dans la base, ceci est l'id à supprimer si l'upload est ok
var globalSyncInProgress = false;       // Pour savoir si on est en phase de synchronisation ou pas (gestion différente du "formulaire" d'upload)
var globalLocalFileContent = undefined; // Le contenu du fichier PDF ou Image a envoyer
var globalSyncObject = [];              // Objet qui stocke tout ce qui reste à synchroniser
var nbStep = 1;
var globalMyNavigator = undefined;
var globalResumable = undefined;
var globalProgressBar = undefined;
var loginAPIKeyErrorInProgress = false; //passe a true dans le cas particulier d'une tentative de login avec une clé d'api qui ne marche pas

//liste des donnees que le serveur peut eventuellement échanger avec nous (factorisation de code)
var globalSrvKeys = ['email', 'api_token', 'firstname', 'name', 'official_app_version', 'official_app_message' ];
