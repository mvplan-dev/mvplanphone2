/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

function gasLabel(oxy, he) {
  if (oxy == 21 && he == 0) {
    return 'Air';
  }
  if (oxy > 0 && he == 0) {
    return "Nitrox " + oxy + "%";
  }
  if(oxy + he == 100) {
    return "Heliox " + oxy + "/" + he;
  }
  return "Trimix " + oxy + "/" + he;
}

function feedGas(objDropDown, objTarget, storageKey, asList = true) {
  myDebug("feedGas key = " + storageKey + ", objDropDown=" + objDropDown + ", objTarget=" + objTarget);
  let htmlCode = "";
  let html = "";
  let htmlSingleGas = "";
  let hasGas = false;
  let hasMultiGas = false;
  let v = localGetData(storageKey);
  if (v == null || v == "") {
    myDebug("feedGas v is null");
    return;
  }

  myDebug("feedGas v is " + v);
  gas = JSON.parse(v);

  for (let i = 0; i < gas.length; i++) {
    // myDebug("feedGas gas #" + i);
    let oxy = gas[i]['oxy'];
    let he = gas[i]['he'];
    let ppo2 = gas[i]['ppo2'];
    let option = oxy + ";" + he + ";" + ppo2 + ";";

    if (oxy !== null) {
      label = gasLabel(oxy, he);

      //Le 1er est checked (sauf si dataLDF voir plus loin)
      let checked = " checked";
      //Si jamais cette boucle a déjà fait un tour on ne coche pas les suivants
      if (hasGas == true) {
        checked = "";
      }

      if (asList) {
        html += ('<div class=\"expandable-content list-item__expandable-content\"><a href="#" onclick="selectGas(\'' + objDropDown + '\',\'' + objTarget + '\',\'' + oxy + '\',\'' + he + '\',\'' + ppo2 + '\');">' + label + '</a></div>');
      } else {
        //Liste de boutons pour simplifier le "clic" sur la page qui affiche tous les mélanges dispo
        html += ('<ons-button onclick="selectGas(\'' + objDropDown + '\',\'' + objTarget + '\',\'' + oxy + '\',\'' + he + '\',\'' + ppo2 + '\');">' + label + '</ons-button>');
      }

      htmlSingleGas = option;
      //Si il y en a déjà au moins un alors multiple :=)
      if (hasGas == true) {
        hasMultiGas = true;
      }
      hasGas = true;
    }
  }

  if (hasGas == false) {
    htmlCode = "<p id=\"gas\">Vous n'avez pas encore de gas, veuillez <a href=\"#\" onclick=\"gotoPage('config-addGas.html');\">configurer vos gas</a></p>";
  } else {
    htmlCode += html;
  }
  return htmlCode;
}

//On insere les donnnes dans l'objet formField a partir de ce qui est contenu dans storageKey
function feedGasConfig(formField, storageKey) {
  myDebug("Call feedGasConfig ...");
  $(formField).empty();
  let v = localGetData(storageKey);
  myDebug("feedGasConfig v is " + v);
  if (v !== null && v !== "") {
    let gas = JSON.parse(v);
    let html = "";
    for (var i = 0; i < gas.length; i++) {
      let oxy = gas[i]['oxy'];
      let he = gas[i]['he'];
      let ppo2 = gas[i]['ppo2'];
      let label = gasLabel(oxy, he);
      if (oxy !== null) {
        html += "<ons-row class=\"listGas\">";
        html += "<ons-col class=\"colListGas\" width='85%'><ons-button class=\"btnListGas\" onclick=\"edGas('" + oxy + "','" + he + "','" + ppo2 + "'," + i + ");feedGasConfig('" + formField + "','" + storageKey + "');\">" + label + "</ons-button></ons-col><ons-col width='10%'><ons-button onclick=\"rmGas('" + storageKey + "'," + i + ");feedGasConfig('" + formField + "','" + storageKey + "');\"><i class=\"zmdi zmdi-delete\"></i></ons-button></ons-col>\n";
        html += "</ons-row>";
      }
    }
    myDebug("feedGasConfig append html on " + formField);
    $(formField).append(html);
  }
  myDebug("end Call feedGasConfig ...");
}


//On insere les donnnes dans l'objet formField a partir de ce qui est contenu dans storageKey
function feedDivesConfig(formField, storageKey) {
  myDebug("Call feedDivesConfig ...");
  $(formField).empty();
  let v = localGetData(storageKey);
  myDebug("feedDivesConfig v is " + v);
  if (v !== null && v !== "") {
    let dives = JSON.parse(v);
    let html = "";
    for (var i = 0; i < dives.length; i++) {
      let label = dives[i]['label'];
      let uuid = dives[i]['uuid'];
      if (label !== null) {
        html += "<ons-row class=\"listDives\">";
        html += "<ons-col class=\"colListDives\" width='85%'><ons-button class=\"btnListDives\" onclick=\"edDive('" + label + "','" + uuid + "');\">" + label + "</ons-button></ons-col><ons-col width='10%'><ons-button onclick=\"rmDive('" + storageKey + "'," + i + ");feedDivesConfig('#feedDives', 'dives');\"><i class=\"zmdi zmdi-delete\"></i></ons-button></ons-col>\n";
        html += "</ons-row>";
      }
    }
    myDebug("feedGasConfig append html on " + formField);
    $(formField).append(html);
  }
  myDebug("end Call feedGasConfig ...");
}


function feedDiveSegments(formField, storageKey) {
  myDebug("Call feedDiveSegments ...");
  $(formField).empty();
  let v = localGetData(storageKey);
  myDebug("feedDiveSegments v is " + v);
  if (v !== null && v !== "") {
    let segments = JSON.parse(v);
    let html = "";
    for (var i = 0; i < segments.length; i++) {
      let segmentUUID = segments[i]['segmentUUID'];
      let depth = segments[i]['depth'];
      let time = segments[i]['time'];
      let setpoint = segments[i]['setpoint'];
      let number = segments[i]['number'];

      let oxy = segments[i]['oxy'];
      let he = segments[i]['he'];
      let ppo2 = segments[i]['ppo2'];

      let label = time + 'min @' + depth + 'm (' + gasLabel(oxy, he) + ", sp=" + setpoint + ')';

      if (oxy !== null) {
        html += "<ons-row class=\"listSegments\">";
        html += "<ons-col class=\"colListSegements\" width='85%'><ons-button class=\"btnListSegments\" onclick=\"edDiveSegment('" + depth + "','" + time + "','" + setpoint + "','" + segmentUUID + "','" + number + "','" + oxy + "','" + he + "','" + ppo2 + "'," + i + ");\">" + label + "</ons-button></ons-col><ons-col width='10%'><ons-button onclick=\"rmDiveSegment('" + storageKey + "'," + i + ");feedDiveSegments('#feedDiveSegments', 'dive-" + globalDiveEd.id + "');\"><i class=\"zmdi zmdi-delete\"></i></ons-button></ons-col>\n";
        html += "</ons-row>";
      }
    }
    myDebug("feedDiveSegments append html on " + formField);
    $(formField).append(html);
  }
  myDebug("end Call feedDiveSegments ...");
}

function feedAproposVersion() {
  $("#feedAproposVersion").empty();
  $("#feedAproposVersion").append("Version : " + globalAppVersion + " rev " + __REVISION__);
}

//Ajout du message custom envoyé par le serveur sur la boite à propos
function feedAproposCustom() {
  myDebug("feedAproposCustom");
  $("#feedAproposCustom").empty();
  myDebug("feedAproposCustom empty ok -> feed with html now");

  $("#feedAproposCustom").append(localGetData('customAbout'));
  return;
}

//Ajoute quelques informations
function feedMvplanInfos() {
  let htmlInfos = "";

  myDebug("feedMvplanInfos");
  $("#feedMvplanInfos").empty();
  myDebug("feedMvplanInfos empty ok -> feed with html now");

  htmlInfos += "<p>";
  htmlInfos += "Compte " + localGetData("email") + "<br />";
  htmlInfos += "Serveur " + localGetData("name_server") + "<br />";
  htmlInfos += "Version " + globalAppVersion + "<br />";
  htmlInfos += "</p>";

  $("#feedMvplanInfos").append(htmlInfos);
  myDebug("feedMvplanInfos append: " + htmlInfos);
  myDebug("feedMvplanInfos append, hide and show ok");
  return htmlInfos;
}

//Ajoute l'adresse mail du compte
function feedMvplanAccount() {
  let htmlInfos = "";
  $("#feedMvplanAccount").empty();
  htmlInfos += translateStrings('account') + " : " + localGetData("email");

  $("#feedMvplanAccount").append(htmlInfos);
  myDebug("feedMvplanAccount append: " + htmlInfos);
  return htmlInfos;
}


/**
 * complète la liste automatiquement
 */
function feedList(fieldName, sourceListName, search = "") {
  myDebug("feedList : " + fieldName);

  let formField = '#' + fieldName;
  let html = "";
  let strRegEx = new RegExp(search, 'i');
  $(formField).empty();

  let thelist = JSON.parse(localGetData(sourceListName));
  if (thelist !== null) {
    thelist.sort(function (a, b) {
      return a.label.localeCompare(b.label);
    });
    //On re-sauvegarde pour avoir la liste triée et donc les indices correspondants (pour del/update)
    localStoreData(sourceListName, JSON.stringify(thelist));

    myDebug("Liste non vide : " + thelist.length);
    for (let f = 0; f < thelist.length; f++) {
      let ltitle = thelist[f].label;
      // let llabel = thelist.arrayData[f][''];
      let llabel = "";
      if (search == "") {
        myDebug("Search vide, ajout d'une entree : " + ltitle + " / " + llabel);
        html += makeOneListEntry(f, thelist[f], sourceListName);
      }
      else {
        myDebug("Vérifie si l'entrée matche avec la recherche ... " + ltitle + " search " + strRegEx);
        if (ltitle.match(strRegEx)) {
          myDebug("Ca matche ! Ajout d'une entree : " + ltitle + " / " + llabel);
          html += makeOneListEntry(f, thelist[f], sourceListName);
        }
      }
    }
  }
  // myDebug("Ajout du code HTML : " + html);

  $(formField).append(html);
}


/**
 * Clic sur un element de la liste de choix 
 */
function listChooseVal(id, sourceListName, code) {
  let val = $('#txtvalue-' + id).text();
  let checkedBoxes = document.querySelectorAll('ons-list-item.expanded');
  //Si c'est une liste repliable, lancer le replis
  if (checkedBoxes.length > 0) {
    checkedBoxes[0].hideExpansion();
  }
  //S'il y a du texte a afficher dans l'entrée de base
  if ($('#labelChooseTag')) {
    $('#labelChooseTag').text(val);
  }

  //S'il y a un code il faut le stocker qqpart pour le joindre au submit du form
  if (sourceListName == 'tags') {
    globalTagFrais = code;
  }
}

