/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/


function addDive(storageKey, label, uuid = null) {
    if (uuid === null || uuid == '' || uuid == undefined) {
        uuid = Math.abs(label.hashCode());
    }
    myDebug("Add Dive for uuid=" + uuid);

    return new Promise((resolve, reject) => {
        let DiveNew = { "uuid": uuid, "label": label };
        let v = localGetData(storageKey);
        let DiveList = null;
        let duplicate = false;
        if (null !== v && v != "") {
            DiveList = JSON.parse(v);
        }
        if (DiveList === null) {
            DiveList = [];
        } else {
            DiveList.forEach(function (item) {
                if (item.uuid == uuid) {
                    duplicate = true;
                    myDebug("Duplicate for " + label);
                }
            });
        }
        myDebug("duplicate Dive " + duplicate);
        if (duplicate === false) {
            DiveList.push(DiveNew);
            myDebug("Add Dive " + label);
            resolve(localStoreData(storageKey, JSON.stringify(DiveList)));
        }
    });
}

function rmDive(storageKey, id) {
    let DiveList = JSON.parse(localGetData(storageKey));
    DiveList.splice(id, 1);
    if (DiveList.length > 0)
        localStoreData(storageKey, JSON.stringify(DiveList));
    else
        localStoreData(storageKey, null);

    //il faut aussi supprimer les données des segments
    
}

// function findDive(storageKey, uuid) {
//     let DiveList = JSON.parse(localGetData(storageKey));
//     if (DiveList === null) {
//         DiveList = [];
//     } else {
//         DiveList.forEach(function (item) {
//             if (item.uuid == uuid) {
//                 let Dive = { "uuid": uuid, "oxy": item.oxy, "he": item.he, "ppo2": item.ppo2 };
//                 return Dive;
//             }
//         });
//     }
// }

/**
 * Creation de l'appel vers la page de modification de la plongée
 */
function edDive(label, id) {
    myDebug("edDive avec label=" + label + ", uuid=" + id);
    globalDiveEd = { id: id, label: label };
    myDebug("edDive call gotopage config-addDive");
    gotoPage('config-addDive.html', false);
    //Q: Possible de modifier le contenu de la page ?
}

// function selectDive(objDropDown, objTarget, oxy, he, ppo2) {
//     myDebug("select Dive " + oxy + " he=" + he + ", ppo2=" + ppo2);
//     globalDiveEd = { oxy: oxy, he: he, ppo2: ppo2 };
//     $(objDropDown).hide();
//     $(objTarget).html(DiveLabel(oxy, he));
//     document.querySelector(objDropDown).hideExpansion();
//     $(objDropDown).show();
// }


// Les segments

function addDiveSegment(diveEd, depth, time, setpoint, oxy, he, ppo2, number, segmentUUID = null) {
    if (segmentUUID === null || segmentUUID == '') {
        let label = diveEd.id + ';' + depth + ';' + time + ';' + setpoint + ';' + oxy + ';' + he + ';' + ppo2;
        segmentUUID = Math.abs(label.hashCode());
    }
    myDebug("Add DiveSegment for dive id=" + diveEd.id + ", segmentUUID=" + segmentUUID);

    return new Promise((resolve, reject) => {
        let storageKey = 'dive-' + diveEd.id;
        let DiveNewSegment = { "segmentUUID": segmentUUID, "depth": depth, "time": time, "setpoint": setpoint, "oxy": oxy, "he": he, "ppo2": ppo2, "number": number };
        let v = localGetData(storageKey);
        let DiveSegmentList = null;
        let duplicate = false;
        if (null !== v && v != "") {
            DiveSegmentList = JSON.parse(v);
        }
        if (DiveSegmentList === null) {
            DiveSegmentList = [];
        } else {
            DiveSegmentList.forEach(function (item) {
                if (item.depth == depth && item.time == time && item.setpoint == setpoint && item.oxy == oxy && item.he == he && item.ppo2 == ppo2) {
                    duplicate = true;
                    myDebug("Duplicate dive segment for " + item.depth);
                }
            });
        }
        myDebug("duplicate Dive " + duplicate);
        if (duplicate === false) {
            DiveSegmentList.push(DiveNewSegment);
            myDebug("Add DiveSegment ");
            resolve(localStoreData(storageKey, JSON.stringify(DiveSegmentList)));
        }
    });
}

function rmDiveSegment(storageKey, id) {
    let DiveSegmentList = JSON.parse(localGetData(storageKey));
    DiveSegmentList.splice(id, 1);
    if (DiveSegmentList.length > 0)
        localStoreData(storageKey, JSON.stringify(DiveSegmentList));
    else
        localStoreData(storageKey, null);
}
