/*
 * (c) Éric Seigne <eric.seigne@videosub.fr> - 2019 - GNU AGPLv3
*/

function rmTank(storageKey, id) {
    let TankList = JSON.parse(localGetData(storageKey));
    TankList.splice(id, 1);
    if (TankList.length > 0)
        localStoreData(storageKey, JSON.stringify(TankList));
    else
        localStoreData(storageKey, null);
}

function addTank(storageKey, name, vol, pres, gasuuid, uuid) {
    return new Promise((resolve, reject) => {
        let TankNew = { "oxy": oxy, "he": he, "ppo2": ppo2 };
        let v = localGetData(storageKey);
        let TankList = null;
        let duplicate = false;
        if (null !== v && v != "") {
            TankList = JSON.parse(v);
        }
        if (TankList === null) {
            TankList = [];
        } else {
            TankList.forEach(function (item) {
                if (item.oxy == oxy && item.he == he && item.ppo2 == ppo2) {
                    duplicate = true;
                    myDebug("Duplicate for " + item.oxy);
                }
            });
        }
        if (duplicate === false) {
            TankList.push(TankNew);
            myDebug("Add Tank " + oxy);
            resolve(localStoreData(storageKey, JSON.stringify(TankList)));
        }
    });
}

/**
 * Creation de l'appel vers la page de modification du Tank
 * @param {*} oxy
 * @param {*} he
 * @param {*} ppo2
 * @param {*} id
 */
function edTank(oxy, he, ppo2, id) {
    myDebug("edTank avec oxy=" + oxy + ", he=" + he + ", ppo2=" + ppo2);
    globalTankEd = { oxy: oxy, he: he, ppo2: ppo2, id: id };
    gotoPage('config-addTank.html');
    //Q: Possible de modifier le contenu de la page ?
}

function selectTank(oxy, he, ppo2) {
    myDebug("select Tank " + oxy + " he=" + he + ", ppo2=" + ppo2);
    $('#listeTank').hide();
    $('#selectedTank').html(label);
    document.querySelector('#listeTank').hideExpansion();
    $('#listeTank').show();
}
