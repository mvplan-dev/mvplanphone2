# Application Mvplan pour smartphone

Le code source est ici !-)

## Merci pour vos dons

N'hésitez pas à donner un coup de pouce au développeur : https://liberapay.com/erics/

## Dev avec Chromium ...

`chromium --disable-web-security --allow-file-access-from-files --user-data-dir=/tmp www/index.html`

Ou encore plus simple:

`cordova run browser`